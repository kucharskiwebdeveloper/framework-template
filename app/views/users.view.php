<?php require ('template/head.php') ?>

    <h1>All users</h1>

<?php foreach ($users as $user): ?>
    <li><?= $user->name ?></li>
<?php endforeach; ?>

<h3>Submit new name</h3>

    <form method="POST" action="/users">
        <input type="text" name="name">
        <button type="submit">OK</button>
    </form>

<?php require ('template/foot.php') ?>