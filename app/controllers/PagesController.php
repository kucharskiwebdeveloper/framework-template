<?php

namespace App\Controllers;

class PagesController
{
    public function home()
    {
        return view('index');
    }

    public function about()
    {
        $title = 'Test app';

        return view('about', compact('title'));
    }

    public function contact()
    {
        $title = 'Test app';

        return view('contact', compact('title'));
    }

}