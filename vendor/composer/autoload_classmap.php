<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'App\\Controllers\\PagesController' => $baseDir . '/app/controllers/PagesController.php',
    'App\\Controllers\\UsersController' => $baseDir . '/app/controllers/UsersController.php',
    'App\\Core\\App' => $baseDir . '/core/App.php',
    'App\\Core\\Services\\Request' => $baseDir . '/core/Request.php',
    'App\\Core\\Services\\Router' => $baseDir . '/core/Router.php',
    'ComposerAutoloaderInite41633f08c4f031b6c9db39b1de5913b' => $vendorDir . '/composer/autoload_real.php',
    'Composer\\Autoload\\ClassLoader' => $vendorDir . '/composer/ClassLoader.php',
    'Composer\\Autoload\\ComposerStaticInite41633f08c4f031b6c9db39b1de5913b' => $vendorDir . '/composer/autoload_static.php',
    'Connection' => $baseDir . '/core/database/Connection.php',
    'QueryBuilder' => $baseDir . '/core/database/QueryBuilder.php',
    'Task' => $baseDir . '/Task.php',
);
